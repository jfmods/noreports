package io.gitlab.jfronny.noreports;

import org.slf4j.*;

public class NoReports {
    public static final Logger LOGGER = LoggerFactory.getLogger("no-reports");
}
