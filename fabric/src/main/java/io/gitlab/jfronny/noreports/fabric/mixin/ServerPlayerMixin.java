package io.gitlab.jfronny.noreports.fabric.mixin;

import io.gitlab.jfronny.noreports.*;
import net.minecraft.network.chat.*;
import net.minecraft.network.protocol.*;
import net.minecraft.network.protocol.game.*;
import net.minecraft.resources.*;
import net.minecraft.server.level.*;
import net.minecraft.server.network.*;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;

@Mixin(ServerPlayer.class)
public abstract class ServerPlayerMixin {
    @Shadow protected abstract int resolveChatTypeId(ResourceKey<ChatType> resourceKey);

    @Redirect(method = "sendChatMessage(Lnet/minecraft/network/chat/PlayerChatMessage;Lnet/minecraft/network/chat/ChatSender;Lnet/minecraft/resources/ResourceKey;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/network/ServerGamePacketListenerImpl;send(Lnet/minecraft/network/protocol/Packet;)V"))
    void noreports$extractChatMessage(ServerGamePacketListenerImpl listener, Packet<?> packet) {
        if (packet instanceof ClientboundPlayerChatPacket chat) {
            Component component = chat.unsignedContent().orElse(chat.signedContent());
            component = ChatTypeDecoration.withSender("chat.type.text").decorate(component, chat.sender());
            listener.send(new ClientboundSystemChatPacket(component, resolveChatTypeId(ChatType.SYSTEM)));
        } else {
            NoReports.LOGGER.warn("Unexpected packet type in sendChatMessage, skipping");
        }
    }
}
