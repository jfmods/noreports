package io.gitlab.jfronny.noreports.fabric;

import io.gitlab.jfronny.noreports.*;
import net.fabricmc.api.*;

public class FabricEntry implements ModInitializer {
    @Override
    public void onInitialize() {
        NoReports.LOGGER.info("NoReports initialized for Fabric");
    }
}
